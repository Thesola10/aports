# Contributor: Damian Kurek <starfire24680@gmail.com>
# Maintainer: Damian Kurek <starfire24680@gmail.com>
pkgname=kodi-game-libretro-mupen64plus-nx
pkgver=2.1.0.10
_realname=Leia
pkgrel=0
pkgdesc="N64 emulation library for the libretro API, based on Mupen64Plus"
options="!check" # no test suite
url="https://github.com/kodi-game/game.libretro.mupen64plus-nx"
arch="x86_64" # blocked by libretro-mupen64plus
license="GPL-2.0-or-later"
makedepends="cmake kodi-dev"
depends="kodi-game-libretro libretro-mupen64plus"
source="$pkgname-$pkgver.tar.gz::https://github.com/kodi-game/game.libretro.mupen64plus-nx/archive/$pkgver-$_realname.tar.gz"
builddir="$srcdir/game.libretro.mupen64plus-nx-$pkgver-$_realname"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake/kodi
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
	rm -r "$pkgdir"/usr/lib/kodi/addons/game.libretro.mupen64plus-nx/game.libretro.mupen64plus-nx.so
	ln -s /usr/lib/libretro/mupen64plus_next_libretro.so \
		"$pkgdir"/usr/lib/kodi/addons/game.libretro.mupen64plus-nx/game.libretro.mupen64plus-nx.so
}

sha512sums="e1485e77160754bb190954376a2bf94b6c5cdeffed75ca364e1352c188b78260c81fa723e451f7573c3964cd0abede8cd9c51e84ae8245607e99313a640ad636  kodi-game-libretro-mupen64plus-nx-2.1.0.10.tar.gz"
